var slideshow = (function () {
    var interval,
        currentSlide = 0;

    function slideshowFn(slideshowId, arr) {
        var slideshowEl = document.getElementById(slideshowId);
        if (interval) {
            clearInterval(interval);
            currentSlide = 0;
            console.log('clearInterval', interval);
        }

        slideshowEl.firstElementChild.src = arr[currentSlide];
        interval = setInterval(function () {
            if (currentSlide === arr.length) {
                currentSlide = 0;
            }
            if (currentSlide % 2 === 0) {
                slideshowEl.lastElementChild.src = arr[(currentSlide + 1) % arr.length];
                slideshowEl.firstElementChild.style.display = 'block';
                slideshowEl.lastElementChild.style.display = 'none';
            } else {
                slideshowEl.firstElementChild.style.display = 'none';
                slideshowEl.lastElementChild.style.display = 'block';
                slideshowEl.firstElementChild.src = arr[(currentSlide + 1) % arr.length];
            }
                currentSlide++;
        }, 1000);
    }

    return slideshowFn;
} ());


slideshow('slideshow', imagesArray);